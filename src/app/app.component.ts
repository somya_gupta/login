import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import  {FormControl} from '@angular/forms';
import { Validators} from '@angular/forms';
import { Logincomponent } from './logincomponent';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  obj = new Logincomponent();
  fg: FormGroup;
  submitted : boolean;


  constructor(private fb: FormBuilder) {
    
  }

  ngOnInit() {
    this.fg = this.fb.group({
      firstname: new FormControl(this.obj.fname, [Validators.required]),
      lastname: new FormControl(this.obj.lname, [Validators.required]),
      gender: new FormControl(this.obj.gender, [Validators.required]),
      contactnum: new FormControl(this.obj.cnt,[Validators.required,Validators.pattern("0-9"),Validators.maxLength(10)]),
      empid:new FormControl(this.obj.eid,[Validators.required,Validators.pattern("0-9"),Validators.maxLength(4)]),
      password:new FormControl(this.obj.pass,[Validators.required,Validators.minLength(8),Validators.maxLength(16)]),
      confirmpassword:new FormControl(this.obj.confpass,[Validators.required,Validators.minLength(8),Validators.maxLength(16)])

    })

  }
  get f(){
    return this.fg.controls;
  }
  onsubmit(){
    this.submitted = true;
    if(this.fg.invalid){
      return;
    }
    alert('success!\n' + JSON.stringify(this.fg.value))
  }
}
  

